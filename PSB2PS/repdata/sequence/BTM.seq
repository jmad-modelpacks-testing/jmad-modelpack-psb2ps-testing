option, -echo;
option, info;
option, warn;
!---------------------------------------------------------------------------
! - The sequence starts exactly at the entry point of BT.BHZ10 and ends at
!   the center of the dump core.
!
! - BTM line is split into two parts:
!   1st part: Entry flange of BT.BHZ10 to Entry flange of BTM.BHZ10
!   2nd part: Entry flange of BTM.BHZ10 to Entry flange of BTY.BVT101
!   3rd part: Entry flange of BTY.BVT101 to PSB beam dump
!
!
! - The position of the elements are relative to the start of the BT line.
!   The position of the elements are extracted from the drawing previewdraft5413_ihens031.dwg
!   The assumption here is that the deflection point is the BTP one at 35.3350 m (drawing PSBIHENS0030F-vAF)
!
!---------------------------------------------------------------------------
!
! BTM1 Line:
!
! - Offset to add to get position in reference of the entry point of BT.BHZ10
!   and to take into account beam path in BT.BHZ10
!   Offset = BeamPath (in BT.BHZ10) - GeometricTrajectory/2 (in BT.BHZ10)
!
! *** BT.BHZ10 ***
!
!   The beam path is calculated as following:
!   The deflection angle is 0.16191966464079602 rad (calculated with Mathematica using the available survey as February 2016)
!
!   BeamPath                = Magnetic length * DeflectionAngle     / Sin( DeflectionAngle     )
!   1.5337933797575676   m  = 1.5271          * 0.16191966464079602 / Sin( 0.16191966464079602 ) m
!   BeamPath/2
!   1.5337933797575676/2 m = 0.7668966898787838 m
!
!   GeometricTrajectory    = Magnetic length /( Cos( DeflectionAngle    /2 ) * Cos( DeflectionAngle    /2 ) )
!   1.5371532690475611   m = 1.5271          /( Cos( 0.16191966464079602/2 ) * Cos( 0.16191966464079602/2 ) ) m
!   GeometricTrajectory/2
!   1.5371532690475611/2 m = 0.76857663452378055 m
!
!
!   BeamPath             - GeometricTrajectory/2
!   1.5337933797575676   - 0.76857663452378055   =  0.76521674523378705 m
!
!---------------------------------------------------------------------------
!
! BTM2 Line:
!
! *** BTM.BHZ10 ***
!
! - Offset to add to take into account beam path in BTM.BHZ10
!   Offset = BeamPath (in BTM.BHZ10) - GeometricTrajectory/2 (in BTM.BHZ10)
!
!   The beam path is calculated as following:
!   The deflection angle is 0.35073658086873477 rad (calculated with Mathematica using the available survey as February 2016)
!
!   BeamPath               = Magnetic length * deflection angle   / ( 2 * Sin(DeflectionAngle   /2))
!   2.3510321048030995   m = 2.339          * 0.35073658086873477 / ( 2 * Sin(0.35073658086873477/2))
!   BeamPath/2
!   2.3510321048030995/2 m = 1.17551605240154975 m
!
!   GeometricTrajectory    = Magnetic length / Cos(DeflectionAngle    /2)
!   2.3754335692495365   m = 2.339          / Cos(0.35073658086873477/2)
!   GeometricTrajectory/2
!   2.3754335692495365/2 m = 1.18771678462476825 m
!
!   BeamPath           - GeometricTrajectory/2
!   2.3510321048030995 - 1.18771678462476825 m  = 1.16331532017833125 m
!
!
!-----------------------------------------------------------------------------------
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PSB-PS/2015
! Sequence file created in February 2016 by O. Berrig, G.P. Di Giovanni, and B. Mikulec
!-----------------------------------------------------------------------------------

btbhz10_beam_path  = 1.5271*dBTBHZ10/sin(dBTBHZ10);
btbhz10_geomtraj   = 1.5271/(cos(dBTBHZ10/2)*cos(dBTBHZ10/2));
btbhz10_bp_men_hg  = btbhz10_beam_path  -  btbhz10_geomtraj/2; ! BeamPath - GeometricTrajectory/2

btmbhz10_beam_path = 2.339*dBTMBHZ10/(2*sin(dBTMBHZ10/2));
btmbhz10_geomtraj  = 2.339/cos(dBTMBHZ10/2);
btmbhz10_bp_men_hg = btmbhz10_beam_path - btmbhz10_geomtraj/2; ! BeamPath - GeometricTrajectory/2


! Distance Deflection Points BT.BHZ10-BTM.BHZ10
d_btbhz10_btmbhz10 = 4.64707153132;

! length BTM1 to the entry flange of BTM.BHZ10
len_btm1 = d_btbhz10_btmbhz10 + btbhz10_bp_men_hg - btmbhz10_geomtraj/2;




! ENDBTM_1 is the entry flange of BTM.BHZ10
 BTM_1: sequence, refer=centre, l = len_btm1 ;
         BT.BHZ10        , at =  btbhz10_beam_path/2         ; ! BeamPath/2
         BTM.BTV10       , at =  2.17199895426 + btbhz10_bp_men_hg ;
         BTM.QNO05       , at =  2.75649889985 + btbhz10_bp_men_hg ;
         ENDBTM_1: MARKER, at =  len_btm1                    ;
 endsequence;

! BTM_2 ends at the dump
len_btm2 = 20.0000 + btmbhz10_bp_men_hg ;
 BTM_2: sequence, refer=centre, l = len_btm2;
         BTM.BHZ10       , at =  btmbhz10_beam_path/2         ; ! BeamPath/2
         BTM.QNO10       , at = 1.81509449667 + btmbhz10_bp_men_hg ;
         BTM.DVT10       , at = 2.55009809205 + btmbhz10_bp_men_hg ;
         BTM.DHZ10       , at = 3.01409808558 + btmbhz10_bp_men_hg ;
         BTM.QNO20       , at = 3.69809367572 + btmbhz10_bp_men_hg ;
         BTM.BPM00       , at = 4.31759349664 + btmbhz10_bp_men_hg ;
         BTM.BTV15       , at = 4.62259641835 + btmbhz10_bp_men_hg ;
         BTM.BSGH01      , at = 8.02859568910 + btmbhz10_bp_men_hg ;
         BTM.BSGV01      , at = 8.02859568910 + btmbhz10_bp_men_hg ;
         BTM.BSGH02      , at = 10.5285948824 + btmbhz10_bp_men_hg ;
         BTM.BSGV02      , at = 10.5285948824 + btmbhz10_bp_men_hg ;
         BTM.BPM10       , at = 11.3910923189 + btmbhz10_bp_men_hg ;
         BTM.BSGH03      , at = 13.0285940756 + btmbhz10_bp_men_hg ;
         BTM.BSGV03      , at = 13.0285940756 + btmbhz10_bp_men_hg ;
         BTM.TDU10       , at = len_btm2                      ; ! Artificial for the time being
 endsequence;

! BTM
len_btm = len_btm1 + len_btm2;
 BTM: sequence, refer=entry, l = len_btm;
         BTM_1, at =  0.0000000000000000;
         BTM_2, at =  len_btm1;
 endsequence;

return;
